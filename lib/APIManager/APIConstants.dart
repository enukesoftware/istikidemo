class APIConstants {
  static const BASE_URL = "http://istiki.yiipro.com";
  static const GET_USER_LIST = "https://jsonplaceholder.typicode.com/users";
  static const CREATE_USER = "https://reqres.in/api/users";
  static const GET_ITEMS = BASE_URL + "/get_item_list.php";
}
