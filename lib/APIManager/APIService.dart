import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sampleapp/APIManager/APIConstants.dart';
import 'package:sampleapp/models/Item.dart';
import 'package:sampleapp/models/ItemsResponse.dart';
import '../models/User.dart';

class Services {
//  static const String url = "https://jsonplaceholder.typicode.com/users";

//  static Future<dynamic> get(String url, dynamic header, dynamic body,
//      String methodType, bool isMultipart) async {
//    Map<String, String> headers = new Map();
//    headers.putIfAbsent('Accept', () => 'application/json');
//    try {
//      print("URL:" + url.toLowerCase());
//      print("Header:" + headers.toString());
//      print("BODY:" + body.toString());
//      print("METHOD:" + methodType.toLowerCase());
//      switch (methodType) {
//        case "GET":
//          Uri uri = Uri.http(url, body);
//          print("URI:" + uri.toString());
//          final response = await http.get(uri, headers: headers);
//          print("RESPONSE:" + response.toString());
//          if (response == 200) {
//            return response.body;
//          }
//          break;
//        case "POST":
//          break;
//      }
//    } catch (e) {
//      return e;
//    }
//  }

//  static Future<List<Item>> getItems2() async {
//    Map<String, String> headers = new Map();
//    headers.putIfAbsent('Accept', () => 'application/json');
//    final resBody = await get(APIConstants.GET_ITEMS, headers,
//        {"email_address": "debal.chakr@gmail.com"}, "GET", false);
//    if (resBody != null) {
//      print("RESBODY:");
//      print(resBody);
//      final List<Item> items = itemFromJson(resBody);
//      return items;
//    } else {
//      return resBody;
//    }
//  }

  static Future<List<User>> getUsers() async {
    try {
      final response = await http.get(APIConstants.GET_ITEMS);
      print("RES:" + response.body);
      if (response.statusCode == 200) {
        final List<User> users = userFromJson(response.body);
        return users;
//        return List<User>();
      } else {
        return List<User>();
      }
    } catch (e) {
      print(e);
      return List<User>();
    }
  }

  static Future<List<Item>> getItems() async {
    Map<String, String> headers = new Map();
    headers.putIfAbsent('Accept', () => 'application/json');
    headers.putIfAbsent('Content-Type', () => 'application/json');
    try {
//      Uri uri = Uri.http(APIConstants.GET_USER_LIST, ""{"email_address": "debal.chakr@gmail.com"});
      final response = await http.post(APIConstants.GET_ITEMS,
          headers: headers,
          body: '{"email_address": "debal.chakr@gmail.com"}',
          encoding: Encoding.getByName('utf-8'));
      if (response.statusCode == 200) {
        final ItemsResponse itemResponse = itemsResponseFromJson(response.body);
        return itemResponse.results;
      } else {
        return List<Item>();
      }
    } catch (e) {
      print(e);
      return List<Item>();
    }
  }
}
