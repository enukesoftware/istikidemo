import 'package:flutter/material.dart';
import 'package:sampleapp/routes/RouteConstants.dart';
import 'package:sampleapp/utils/Images.dart';
import 'package:sampleapp/utils/colors.dart';
import 'package:sampleapp/utils/sizes.dart';

class HomePage extends StatefulWidget {
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  TextStyle styleBold =
      TextStyle(fontSize: Sizes.LARGE, fontWeight: FontWeight.bold);
  TextStyle styleNormal =
      TextStyle(fontSize: Sizes.LARGE, fontWeight: FontWeight.w400);

  @override
  Widget build(BuildContext context) {
    final sizeBox = SizedBox(
      height: 5,
    );

    return Scaffold(
      backgroundColor: AppColor.BACKGROUND_PAGE,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    height: 50.0,
                    child: Image.asset(
                      Images.login_logo,
                      fit: BoxFit.contain,
                    ),
                  ),
                  IconButton(
                    iconSize: 30,
                    padding: EdgeInsets.all(0),
                    icon: Icon(Icons.info_outline),
                    onPressed: () {},
                  ),
                ],
              ),
              Flexible(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Scan to add or view an item",
                      style: styleBold,
                    ),
                    SizedBox(height: 5),
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      height: 100,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: AppColor.BORDER_COLOR,
                        ),
                      ),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: GestureDetector(
                              child: Image.asset(
                                Images.qrCode,
                                fit: BoxFit.contain,
                              ),
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed(AddItemPageRoute);
                              }),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      "Find items without scanning",
                      style: styleBold,
                    ),
                    SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(40.0, 0, 10, 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          GestureDetector(
                            child: Text(
                              "Alphabetical List",
                              style: styleNormal,
                            ),
                            onTap: () {},
                          ),
                          sizeBox,
                          GestureDetector(
                            child: Text(
                              "Search",
                              style: styleNormal,
                            ),
                            onTap: () {},
                          ),
                          sizeBox,
                          GestureDetector(
                            child: Text(
                              "By Folder",
                              style: styleNormal,
                            ),
                            onTap: () {},
                          ),
                          sizeBox,
                          GestureDetector(
                            child: Text(
                              "By Location",
                              style: styleNormal,
                            ),
                            onTap: () {},
                          ),
                          sizeBox,
                        ],
                      ),
//                        child: GestureDetector(
//                          child: Text(
//                            "Alphabetical List",
//                            style: styleNormal,
//                          ),
//                        ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
