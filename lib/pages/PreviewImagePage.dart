import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sampleapp/models/PreviewMediaData.dart';
import 'package:vector_math/vector_math_64.dart' show Vector3;

class PreviewImagePage extends StatefulWidget {
//  final String tag;
//  final String url;
//
//  PreviewImagePage({Key key, @required this.tag, @required this.url})
//      : assert(tag != null),
//        assert(url != null),
//        super(key: key);
  PreviewImagePageState createState() => PreviewImagePageState();
}

class PreviewImagePageState extends State<PreviewImagePage> {
  double _scale = 1.0;
  double _previousScale = 1.0;

  @override
  Widget build(BuildContext context) {
    final PreviewMediaData args = ModalRoute.of(context).settings.arguments;

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Image',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: GestureDetector(
        onScaleStart: (ScaleStartDetails details) {
          _previousScale = _scale;
          setState(() {});
        },
        onScaleUpdate: (ScaleUpdateDetails details) {
          _scale = _previousScale * details.scale;
          setState(() {});
        },
        onScaleEnd: (ScaleEndDetails details) {
          _previousScale = 1.0;
          setState(() {});
        },
        child: Hero(
          tag: args?.tag,
          child: Transform(
            alignment: FractionalOffset.center,
            transform: Matrix4.diagonal3(Vector3(_scale, _scale, _scale)),
            child: Center(
              child: args.data == null
                  ? null
                  : Image.file(
                      File(args?.data),
                      fit: BoxFit.cover,
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
