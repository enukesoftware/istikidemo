import 'dart:io';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

class MyCameraPage extends StatefulWidget {
  MyCameraPageState createState() => MyCameraPageState();
}

class MyCameraPageState extends State<MyCameraPage> {
  File _image;
  File _videoFile;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
//    try {
//      print(pickedFile.path);
//    } catch (e) {
//      print(e.toString());
//    }
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  Future getVideo() async {
    final pickedFile = await picker.getVideo(source: ImageSource.camera);
    try {
      print(pickedFile.path);
    } catch (e) {
      print(e.toString());
    }
    setState(() {
      _videoFile = File(pickedFile.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Camera",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Center(
//          child:_image == null ? Text('No image selected.') : Image.file(_image),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 200.0,
                child: _image == null
                    ? Text(
                        'No image selected.',
                        textAlign: TextAlign.center,
                      )
                    : Image.file(_image),
              ),
              SizedBox(height: 45.0),
              Container(
                color: Colors.orange,
                height: MediaQuery.of(context).size.height * (30 / 100),
                width: MediaQuery.of(context).size.width * (100 / 100),
                child: _videoFile == null
                    ? Text(
                        'No video selected.',
                        textAlign: TextAlign.center,
                      )
                    : FittedBox(
                        fit: BoxFit.contain,
                        child: mounted ? Chewie(
                          controller: ChewieController(
                            videoPlayerController: VideoPlayerController.file(_videoFile),
                            aspectRatio: 2/2,
                            autoPlay: true,
                            looping: true,
                          ),
                        ) : Container(),
                      ),
              )
            ],
          ),
//        ),
        ),
//        floatingActionButton: FloatingActionButton(
//          onPressed: getImage,
//          tooltip: 'Pick Image',
//          child: Icon(Icons.add_a_photo),
//        ),
        floatingActionButton:
            Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          FloatingActionButton(
            child: Icon(Icons.camera_enhance),
            onPressed: getImage,
            heroTag: null,
          ),
          SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            child: Icon(Icons.videocam),
            onPressed: getVideo,
            heroTag: null,
          )
        ]));
  }
}
