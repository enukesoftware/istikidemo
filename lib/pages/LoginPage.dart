import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sampleapp/pages/RandomWords.dart';
import 'package:sampleapp/routes/RouteConstants.dart';
import 'package:sampleapp/utils/Images.dart';

class LoginPage extends StatefulWidget {
//  LoginPage({Key key, this.title}) : super(key: key);
//
//  final String title;
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String emailErrorText;
  String passwordErrorText;
  String emailAddress;
  String passwordValue;
  TextStyle style = TextStyle(fontSize: 16.0);

  @override
  void dispose() {
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  initState() {
//    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    super.initState();
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  bool validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  bool isValid() {
    bool validate = true;
    if (emailAddress == null || emailAddress.isEmpty) {
      setState(() {
        emailErrorText = 'Please enter email address';
      });
      validate = false;
    } else if (!validateEmail(emailAddress)) {
      setState(() {
        emailErrorText = 'Please enter valid email address';
      });
      validate = false;
    }

    if (passwordValue == null || passwordValue.isEmpty) {
      setState(() {
        passwordErrorText = 'Please enter password';
      });
      validate = false;
    } else if (!validatePassword(passwordValue)) {
      setState(() {
        passwordErrorText =
            'Password must contain at least one uppercase letter, one lowercase letter, one number, one special character and be longer than six characters';
      });
      validate = false;
    }
    return validate;
  }

  void onLoginClick() {
//    utility.hideKeyboard();
//    if (isValid()) {
    // If the form is valid, display a snackbar. In the real world,
    // you'd often call a server or save the information in a database.

//            Scaffold.of(context)
//                .showSnackBar(SnackBar(content: Text('Processing Data')));
//      Navigator.push(
//          context, MaterialPageRoute(builder: (context) => RandomWords()));
    Navigator.of(context).pushNamed(HomePageRoute);
//    }
  }

  Widget build(BuildContext buildContext) {
    final loginLabel = Text("Login here to your existing iStiki account",
        style: TextStyle(fontSize: 14));

    final emailField = TextField(
      obscureText: false,
      style: style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
        hintText: "Email Address",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        errorText: emailErrorText,
      ),
      textInputAction: TextInputAction.next,
      onSubmitted: (_) => FocusScope.of(context).nextFocus(),
      onChanged: (value) {
        setState(() {
          emailAddress = value;
          emailErrorText = null;
        });
      },
    );

    final passwordField = TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
        hintText: "Password",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        errorMaxLines: 3,
        errorText: passwordErrorText,
      ),
      textInputAction: TextInputAction.done,
      onSubmitted: (_) => {onLoginClick()},
      onChanged: (value) {
        setState(() {
          passwordValue = value;
          passwordErrorText = null;
        });
      },
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(10.0),
      color: Theme.of(context).primaryColor,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          onLoginClick();
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
//      appBar: AppBar(
//        title: Text(
//          'Login',
//          style: TextStyle(color: Colors.white),
//        ),
//        centerTitle: true,
//      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: ListView(
            padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
//            mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 50.0,
                    child: Image.asset(
//                  "assets/images/iStiki_login_logo.png",
                      Images.login_logo,
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(height: 40.0),
                  Text("Login", style: TextStyle(fontSize: 30)),
                  SizedBox(height: 10.0),
                  loginLabel,
                  SizedBox(height: 25.0),
                  emailField,
                  SizedBox(height: 10.0),
                  passwordField,
                  SizedBox(
                    height: 30.0,
                  ),
                  loginButon,
                  SizedBox(
                    height: 20.0,
                  ),
                  GestureDetector(
                    child: Text(
                      "Forgot password ? ",
                      style: TextStyle(fontSize: 14),
                    ),
                    onTap: () {},
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
