import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sampleapp/models/PreviewMediaData.dart';
import 'package:video_player/video_player.dart';

class PreviewVideoPage extends StatefulWidget {
//  final String tag;
//  final String url;
//
//  PreviewImagePage({Key key, @required this.tag, @required this.url})
//      : assert(tag != null),
//        assert(url != null),
//        super(key: key);
  PreviewVideoPageState createState() => PreviewVideoPageState();
}

class PreviewVideoPageState extends State<PreviewVideoPage> {
  VideoPlayerController _videoPlayerController;
  Future<void> futureController;
  String _videoPath;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if(_videoPlayerController != null){
      _videoPlayerController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final PreviewMediaData args = ModalRoute.of(context).settings.arguments;
    if(_videoPlayerController == null){
      _videoPlayerController = args.videoPlayerController;
//      if(args.data.startsWith("http")){
//        _videoPlayerController = VideoPlayerController.network(args.data);
//      }else{
//        _videoPlayerController = VideoPlayerController.file(File(args.data));
//      }
//      _videoPlayerController.setLooping(true);
      futureController = args.futureController;//_videoPlayerController.initialize();
//    _videoPath = args.data;
//    _videoPlayerController.setLooping(true);

//      _videoPlayerController.seekTo(args.videoPlayerController.value.duration);
    }

//    _videoPlayerController.play();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Video',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            futureController != null
                ? FutureBuilder(
                    future: futureController,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        return AspectRatio(
                          aspectRatio: _videoPlayerController.value.aspectRatio,
                          child: GestureDetector(
                            /**/
                            onTap: () {
                              setState(() {
                                print("CLICKED");
                                if (_videoPlayerController != null) {
                                  print("CLICKED2");
                                  if (_videoPlayerController.value.isPlaying) {
                                    print("CLICKED3");
                                    _videoPlayerController.pause();
                                  }
                                }
                              });
                            },
                            child: VideoPlayer(_videoPlayerController),
                          ),
                        );
                      } else {
                        return Center(child: CircularProgressIndicator());
                      }
                    },
                  )
                : null,
            Align(
                alignment: Alignment.center,
                child: (_videoPlayerController != null &&
                        !_videoPlayerController.value.isPlaying)
                    ? Container(
                        width: 50,
                        height: 50,
                        child: RaisedButton(
                          padding: EdgeInsets.all(0),
                          color: Colors.transparent,
                          textColor: Colors.white,
                          onPressed: () {
                            setState(() {
                              if (_videoPlayerController != null) {
                                if (_videoPlayerController.value.isPlaying) {
                                  _videoPlayerController.pause();
                                } else {
                                  _videoPlayerController.play();
                                }
                              }
                            });
                          },
                          child: Icon(Icons.play_arrow, size: 40),
                        ),
                      )
                    : null),
            Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  width: 40,
                  height: 40,
                  margin: EdgeInsets.only(bottom: 5),
                  child: Center(
                    child: RaisedButton(
                      padding: EdgeInsets.all(0),
                      color: Colors.transparent,
                      textColor: Colors.white,
                      onPressed: () {},
                      child: Icon(Icons.crop_square),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
