import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sampleapp/models/PreviewMediaData.dart';
import 'package:sampleapp/routes/RouteConstants.dart';
import 'package:sampleapp/utils/Constants.dart';
import 'package:sampleapp/utils/Images.dart';
import 'package:sampleapp/utils/colors.dart';
import 'package:sampleapp/utils/sizes.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

class AddItemPage extends StatefulWidget {
  AddItemPageState createState() => AddItemPageState();
}

class AddItemPageState extends State<AddItemPage> {
  String itemName;
  String folderValue = "Camping Supplies";
  String addressValue = "123 Main Street, Anytime, USA";
  String locationValue = "Basement";
  String _imagePath; // =
  //"/storage/emulated/0/Android/data/com.enuke.sampleapp/files/Pictures/d1e529b1-27d9-47b2-abc4-3eba11bf7ba52901272490035888315.jpg";
  String _videoPath;
  final picker = ImagePicker();

//  static File videoFile = File(Images.sampleVideo);
  final TextEditingController _itemNameController = new TextEditingController();
  VideoPlayerController
      _videoPlayerController; // = VideoPlayerController.asset(Images.sampleVideo);
  Future<void> futureController;
  TextStyle styleFont = TextStyle(fontSize: Sizes.REGULAR);
  TextStyle styleDate = TextStyle(fontSize: Sizes.EXTRA_SMALL);
  TextStyle styleTitle = TextStyle(fontSize: Sizes.SMALL);
  TextStyle styleUnderLine = TextStyle(decoration: TextDecoration.underline);

  @override
  void initState() {
//    if (_videoPath == null) {
//      _videoPath = "https://www.radiantmediaplayer.com/media/bbb-360p.mp4";
//      _videoPlayerController = VideoPlayerController.network(_videoPath);
//    } else {
//      _videoPlayerController = VideoPlayerController.file(File(_videoPath));
//    }
//
//    _videoPlayerController.setLooping(true);
//    futureController = _videoPlayerController.initialize();
    super.initState();
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    super.dispose();
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    try {
      print(pickedFile.path);
    } catch (e) {
      print(e.toString());
    }
    setState(() {
      _imagePath = pickedFile.path;
//      _imageFile = File(pickedFile.path);
    });
  }

  Future getVideo() async {
    final pickedFile = await picker.getVideo(source: ImageSource.camera);
    try {
      print(pickedFile.path);
    } catch (e) {
      print(e.toString());
    }
    setState(() {
      _videoPath = pickedFile.path;
//      _videoFile = File(pickedFile.path);
      _videoPlayerController =
          VideoPlayerController.file(File(pickedFile.path));
      _videoPlayerController.setLooping(true);
      futureController = _videoPlayerController.initialize();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double statusBarHeight = MediaQuery.of(context).padding.top;

    final itemNameView = Expanded(
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: TextField(
          controller: _itemNameController,
          obscureText: false,
          style: TextStyle(fontSize: Sizes.REGULAR),
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            contentPadding: EdgeInsets.fromLTRB(10.0, 0, 0, 10),
            hintText: "Item Name",
            disabledBorder: InputBorder.none,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
            ),
            suffixIcon: IconButton(
              padding: EdgeInsets.all(0),
              icon: Icon(Icons.close),
              color: Colors.black,
              onPressed: () {
                setState(() {
                  itemName = "";
                });
                _itemNameController.clear();
              },
            ),
          ),
          onChanged: (value) {
            setState(() {
              itemName = value;
            });
          },
        ),
      ),
    );

    final floderAddressView = Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            child: Text(
              "Folder",
              style: styleUnderLine.merge(styleFont),
            ),
          ),
          Text(","),
          Icon(
            Icons.location_on,
            color: Theme.of(context).primaryColor,
          ),
          GestureDetector(
            child: Text(
              "Address",
              style: styleUnderLine.merge(styleFont),
            ),
          ),
          Text("-> "),
          Flexible(
            child: GestureDetector(
              child: Text(
                "Location at address",
                style: styleUnderLine.merge(styleFont),
              ),
            ),
          ),
        ],
      ),
    );

    final mediaIcons = Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, spreadRadius: 1, blurRadius: 1)
                ],
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Theme.of(context).primaryColor),
            child: IconButton(
              icon: Icon(Icons.assignment),
              iconSize: 30,
              color: Colors.white,
              onPressed: () {},
            ),
          ),
          SizedBox(width: 10),
          Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, spreadRadius: 1, blurRadius: 1)
                ],
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Theme.of(context).primaryColor),
            child: IconButton(
              icon: Icon(Icons.mic),
              iconSize: 30,
              color: Colors.white,
              onPressed: () {},
            ),
          ),
          SizedBox(width: 10),
          Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, spreadRadius: 1, blurRadius: 1)
                ],
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Theme.of(context).primaryColor),
            child: IconButton(
              icon: Icon(Icons.camera_alt),
              iconSize: 30,
              color: Colors.white,
              onPressed: () {
                getImage();
              },
            ),
          ),
          SizedBox(width: 10),
          Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12, spreadRadius: 1, blurRadius: 1)
                ],
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: Theme.of(context).primaryColor),
            child: IconButton(
              icon: Icon(Icons.videocam),
              iconSize: 30,
              color: Colors.white,
              onPressed: () {
                getVideo();
              },
            ),
          ),
        ],
      ),
    );

    final folderView = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 15),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: Text.rich(
            TextSpan(
              text: "Folder: ",
              style: styleFont,
              children: <TextSpan>[
                TextSpan(
                  text: "$folderValue",
                  style: styleFont.merge(styleUnderLine),
                  recognizer: new TapGestureRecognizer()..onTap = () {},
                )
              ],
            ),
          ),
        ),
      ],
    );

    final addressView = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 5),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Padding(
            padding: EdgeInsets.fromLTRB(2, 0, 0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.location_on,
                  color: Theme.of(context).primaryColor,
//                  size: 30,
                ),
                Flexible(
                  child: Text(
                    "$addressValue",
                    style: styleFont.merge(styleUnderLine), /**/
                  ),
                ),
              ],
            )),
      ],
    );

    final locationView = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 5),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: Text.rich(
            TextSpan(
              text: "Location: ",
              style: styleFont,
              children: <TextSpan>[
                TextSpan(
                  text: "$locationValue",
                  style: styleFont.merge(styleUnderLine),
                  recognizer: new TapGestureRecognizer()..onTap = () {},
                )
              ],
            ),
          ),
        ),
      ],
    );

    final noteRow = Column(
      children: <Widget>[
        SizedBox(height: 5),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Text(
                  "2017-01-09",
                  style: styleDate,
                ),
              ),
              Expanded(
                flex: 6,
                child: Text(
                  "This is a test note. If the user types a lot it wraps like this. It currently works well.",
                  style: styleFont,
                ),
              ),
              SizedBox(width: 10),
              Container(
                height: 25,
                width: 25,
                decoration: BoxDecoration(
//                  shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12, spreadRadius: 1, blurRadius: 1)
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Theme.of(context).primaryColor),
                child: IconButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  icon: Icon(Icons.edit),
                  iconSize: 15,
                  color: Colors.white,
                  padding: EdgeInsets.all(0),
                  onPressed: () {},
                ),
              ),
              SizedBox(width: 10),
              Container(
                height: 25,
                width: 25,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12, spreadRadius: 1, blurRadius: 1)
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Theme.of(context).primaryColor),
                child: IconButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  icon: Icon(Icons.close),
                  iconSize: 15,
                  color: Colors.white,
                  padding: EdgeInsets.all(0),
                  onPressed: () {},
                ),
              ),
            ],
          ),
        ),
      ],
    );

    final audioRow = Column(
      children: <Widget>[
        SizedBox(height: 5),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Container(
          height: 100,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("2017-01-09", style: styleDate),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text("The title for audio", style: styleTitle),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 6,
                child: Align(
                  alignment: Alignment.center,
                  child: SizedBox(
//                    width: 50,
//                    height: 50,
                    child: Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 1,
                                blurRadius: 1)
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          color: Theme.of(context).primaryColor),
                      child: IconButton(
                        icon: Icon(Icons.mic),
                        iconSize: 30,
                        color: Colors.white,
                        onPressed: () {},
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10),
              Align(
                alignment: Alignment.center,
                child: Container(
                  height: 25,
                  width: 25,
                  decoration: BoxDecoration(
//                  shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 1,
                            blurRadius: 1)
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Theme.of(context).primaryColor),
                  child: IconButton(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    icon: Icon(Icons.close),
                    iconSize: 15,
                    color: Colors.white,
                    padding: EdgeInsets.all(0),
                    onPressed: () {},
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );

    final imageRow = Column(
      children: <Widget>[
        SizedBox(height: 5),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Container(
          height: 150,
          child: Row(
//            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text("2017-01-09", style: styleDate),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text("The title for image", style: styleTitle),
                      ),
                    ],
                  )),
              Expanded(
                flex: 5,
                child: Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 250,
//                    height: 150,
                    child: Center(
                      child: Flexible(
                        child: GestureDetector(
                          onTap: () {
                            PreviewMediaData previewMediaData =
                                PreviewMediaData();
                            previewMediaData.data = _imagePath;
                            previewMediaData.tag = "image";
                            Navigator.pushNamed(context, PreviewImagePageRoute,
                                arguments: previewMediaData);
                          },
                          child: _imagePath == null
                              ? null
                              : Image.file(
                                  File(_imagePath),
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10),
              Align(
                alignment: Alignment.center,
                child: Container(
                  height: 25,
                  width: 25,
                  decoration: BoxDecoration(
//                  shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 1,
                            blurRadius: 1)
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Theme.of(context).primaryColor),
                  child: IconButton(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    icon: Icon(Icons.close),
                    iconSize: 15,
                    color: Colors.white,
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      setState(() {
                        _imagePath = null;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );

    final videoRow = Column(
      children: <Widget>[
        SizedBox(height: 5),
        Divider(
          thickness: 2,
        ),
        SizedBox(height: 5),
        Container(
          height: 150,
          child: Row(
//            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("2017-01-09", style: styleDate),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text("The title for video", style: styleTitle),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 5,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 250,
                        child: Center(
                          child: Flexible(
//                          child:  Center(child:CircularProgressIndicator()),
                            child: futureController != null
                                ? FutureBuilder(
                                    future: futureController,
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.done) {
                                        return AspectRatio(
                                          aspectRatio: _videoPlayerController
                                              .value.aspectRatio,
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                if (_videoPlayerController !=
                                                    null) {
                                                  if (_videoPlayerController
                                                      .value.isPlaying) {
                                                    _videoPlayerController
                                                        .pause();
                                                  }
                                                }
                                              });
                                            },
                                            child: VideoPlayer(
                                                _videoPlayerController),
                                          ),
                                        );
                                      } else {
                                        return Center(
                                            child: CircularProgressIndicator());
                                      }
                                    },
                                  )
                                : null,
                          ),
                        ),
                      ),
                    ),
                    Align(
                        alignment: Alignment.center,
                        child: (_videoPlayerController != null &&
                                !_videoPlayerController.value.isPlaying)
                            ? Container(
                                width: 50,
                                height: 50,
                                child: RaisedButton(
                                  padding: EdgeInsets.all(0),
                                  color: Colors.transparent,
                                  textColor: Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      if (_videoPlayerController != null) {
                                        if (_videoPlayerController
                                            .value.isPlaying) {
                                          _videoPlayerController.pause();
                                        } else {
                                          _videoPlayerController.play();
                                        }
                                      }
                                    });
                                  },
                                  child: Icon(Icons.play_arrow, size: 40),
                                ),
                              )
                            : null
//                      child: IconButton(
//                        icon: Icon(Icons.play_arrow),
//                        iconSize: 50,
//                        color: Theme.of(context).primaryColor,
//                        padding: EdgeInsets.all(0),
//                        onPressed: () {},
//                      ),
                        ),
                    Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          width: 40,
                          height: 40,
                          margin: EdgeInsets.only(bottom: 5),
                          child: Center(
                            child: RaisedButton(
                              padding: EdgeInsets.all(0),
                              color: Colors.transparent,
                              textColor: Colors.white,
                              onPressed: () {
//                                if (_videoPlayerController != null &&
//                                    _videoPlayerController.value.isPlaying) {
//                                  _videoPlayerController.pause();
//                                }
//                                PreviewMediaData previewMediaData =
//                                    PreviewMediaData();
//                                previewMediaData.tag = "video";
//                                previewMediaData.data = _videoPath;
//                                previewMediaData.videoPlayerController =
//                                    _videoPlayerController;
//                                previewMediaData.futureController =
//                                    futureController;
//                                Navigator.pushNamed(
//                                  context,
//                                  PreviewVideoPageRoute,
//                                  arguments: previewMediaData,
//                                );
                              },
                              child: Icon(Icons.crop_square),
                            ),
                          ),
                        )),
                  ],
                ),
              ),
              SizedBox(width: 10),
              Align(
                alignment: Alignment.center,
                child: Container(
                  height: 25,
                  width: 25,
                  decoration: BoxDecoration(
//                  shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 1,
                            blurRadius: 1)
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Theme.of(context).primaryColor),
                  child: IconButton(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    icon: Icon(Icons.close),
                    iconSize: 15,
                    color: Colors.white,
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      setState(() {
                        if (_videoPlayerController != null &&
                            _videoPlayerController.value.isPlaying) {
                          _videoPlayerController.pause();
                        }
                        _videoPath = null;
                        _videoPlayerController = null;
                        futureController = null;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, Constants.TOOLBAR_HEIGHT),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 3, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
//          height: Constants.TOOLBAR_HEIGHT + statusBarHeight,
          child: Container(
            padding: EdgeInsets.fromLTRB(0, statusBarHeight, 0, 0),
            color: Theme.of(context).primaryColor,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(
                      Platform.isAndroid
                          ? Icons.arrow_back
                          : Icons.arrow_back_ios,
                      size: Platform.isIOS ? 25 : 25,
                      color: Colors.white,
                    ),
                    onPressed: () {
//                      Navigator.of(context).pushNamed('/login');
                      Navigator.pop(context);
                    },
                  ),
                  GestureDetector(
                    child: Text(
                      "Item Name: ",
                      style: TextStyle(
                          fontSize: Sizes.MEDIUM, color: Colors.white),
                    ),
                    onTap: () {
//                      setState(() {
//                        locationValue = (locationValue == null)
//                            ? "Basement"
//                            : locationValue + " Basement";
//                        folderValue = (folderValue == null)
//                            ? "Camping Supplies"
//                            : folderValue + " Camping Supplies";
//                        addressValue = (addressValue == null)
//                            ? "123 Main Street, Anytime, USA"
//                            : addressValue + "123 Main Street, Anytime, USA";
//                      });
                    },
                  ),
                  itemNameView,
                  IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(
                      Icons.check,
                      size: 30,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                floderAddressView,
                SizedBox(height: 30),
                mediaIcons,
                if (folderValue != null) folderView,
                if (addressValue != null) addressView,
                if (locationValue != null) locationView,
                noteRow,
                audioRow,
                if (_imagePath != null) imageRow,
                if (_videoPath != null) videoRow
              ],
            ),
          ),
        ),
      ),
    );
  }
}
