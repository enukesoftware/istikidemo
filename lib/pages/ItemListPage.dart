import 'package:flutter/material.dart';
import 'package:sampleapp/models/Item.dart';
import '../APIManager/APIService.dart';
import '../models/User.dart';

class ItemListPage extends StatefulWidget {
  ItemListPage() : super();

  ItemListPageState createState() => ItemListPageState();
}

class ItemListPageState extends State<ItemListPage> {
  List<Item> _users;
  bool _loading;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loading = true;
    Services.getItems().then((items) {
      setState(() {
        _users = items;
        _loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(_loading ? 'Loading...' : 'Home'),
      ),
      body: Container(
        color: Colors.white,
        child: ListView.builder(
            itemCount: _users == null ? 0 : _users.length,
            itemBuilder: (context, index) {
              Item user = _users[index];
              return ListTile(
                title: Text(user.childName),
                subtitle: Text(user.childAge),
                leading: Container(
                  height: 50.0,
                  width: 50.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: (user.imageUrl != null && user.imageUrl != "")?NetworkImage(user.imageUrl):AssetImage("assets/images/iStiki_login_logo.png"),
                        fit: BoxFit.cover
                    ),
                  ),
//                  child: Image.network(
//                    user.imageUrl,
//                    fit: BoxFit.cover,
//                  ),
                ),
              );
            }),
      ),
    );
  }
}
