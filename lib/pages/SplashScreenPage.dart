import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sampleapp/routes/RouteConstants.dart';
import 'package:sampleapp/utils/Images.dart';

class SplashScreenPage extends StatefulWidget {
  SplashScreenPageState createState() => SplashScreenPageState();
}

class SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    // TODO: implement initState
//    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    super.initState();
    Future.delayed(
        Duration(seconds: 3),
        (){
          Navigator.of(context).pushNamedAndRemoveUntil(LoginPageRoute, (Route<dynamic> route) => false);
        }
    );
  }

  @override
  void dispose() {
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Image.asset(
            Images.splash_logo,
            fit: BoxFit.contain,
          ),
        ),
      ),
    );
  }
}
