class Sizes {
  static final LARGE = 20.0;
  static final SEMI_LARGE =18.0;
  static final EXTRA_LARGE =24.0;
  static final MEDIUM=16.0;
  static final REGULAR=14.0;
  static final SMALL=12.0;
  static final EXTRA_SMALL=10.0;
}