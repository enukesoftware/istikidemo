import 'package:flutter/material.dart';

class AppColor {
  static const GRAY = Color(0xFFf5f5f5);
  static const BLACK = Color(0xFF000000);
  static const WHITE = Color(0xFFFFFFFF);
  static final DEVIDER_COLOR = Color(0xFFEBEBEB);
  static final SUCCESS = Colors.green;
  static final BACKGROUND_PAGE = Color(0xFFFFFFFF);
  static const PRIMARY = Color(0xFF3F51B5);
  static const BORDER_COLOR = Color(0xFF000000);

  static Map<int, Color> themeColor = {
    50: Color.fromRGBO(192, 1, 9, .1),
    100: Color.fromRGBO(192, 1, 9, .2),
    200: Color.fromRGBO(192, 1, 9, .3),
    300: Color.fromRGBO(192, 1, 9, .4),
    400: Color.fromRGBO(192, 1, 9, .5),
    500: Color.fromRGBO(192, 1, 9, .6),
    600: Color.fromRGBO(192, 1, 9, .7),
    700: Color.fromRGBO(192, 1, 9, .8),
    800: Color.fromRGBO(192, 1, 9, .9),
    900: Color.fromRGBO(192, 1, 9, 1),
  };

  static MaterialColor themDark = MaterialColor(0xFF3F51B5, themeColor);
}
