class Images {
  static final splash_logo = "assets/images/splash_logo.png";
  static final login_logo = "assets/images/iStiki_login_logo.png";
  static final sample = "assets/images/sample.png";
  static final qrCode = "assets/images/qr_image.png";
//  static final sampleVideo = "assets/video/sample_video.mp4";
}
