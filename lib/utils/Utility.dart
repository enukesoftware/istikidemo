import 'package:flutter/services.dart';

class Utility {
  void hideKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }
}
