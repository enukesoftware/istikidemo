// To parse this JSON data, do
//
//     final item = itemFromJson(jsonString);

import 'dart:convert';

//List<Item> itemFromJson(String str) => List<Item>.from(json.decode(str).map((x) => Item.fromJson(x)));
//
//String itemToJson(List<Item> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

Item itemFromJson(String str) => Item.fromJson(json.decode(str));

String itemToJson(Item data) => json.encode(data.toJson());

class Item {
  Item({
    this.birthDate,
    this.child12,
    this.childAge,
    this.childName,
    this.childUniqueId,
    this.imageUrl,
  });

  String birthDate;
  String child12;
  String childAge;
  String childName;
  String childUniqueId;
  String imageUrl;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    birthDate: json["birth_date"],
    child12: json["child_12"],
    childAge: json["child_age"],
    childName: json["child_name"],
    childUniqueId: json["child_unique_id"],
    imageUrl: json["image_url"],
  );

  Map<String, dynamic> toJson() => {
    "birth_date": birthDate,
    "child_12": child12,
    "child_age": childAge,
    "child_name": childName,
    "child_unique_id": childUniqueId,
    "image_url": imageUrl,
  };
}