import 'dart:convert';

import 'package:sampleapp/models/Item.dart';

ItemsResponse itemsResponseFromJson(String str) => ItemsResponse.fromJson(json.decode(str));

String itemsResponseToJson(ItemsResponse data) => json.encode(data.toJson());

class ItemsResponse {
  ItemsResponse({
    this.message,
    this.status,
    this.results,
  });

  String message;
  int status;
  List<Item> results;

  factory ItemsResponse.fromJson(Map<String, dynamic> json) => ItemsResponse(
    message: json["message"],
    status: json["status"],
    results: List<Item>.from(json["results"].map((x) => Item.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "status": status,
    "results": List<dynamic>.from(results.map((x) => x.toJson())),
  };
}