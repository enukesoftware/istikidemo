import 'package:flutter/material.dart';
import '../APIManager/APIService.dart';
import '../models/User.dart';

class ApiDemo extends StatefulWidget {
  ApiDemo() : super();

  ApiDemoState createState() => ApiDemoState();
}

class ApiDemoState extends State<ApiDemo> {
  List<User> _users;
  bool _loading;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loading = true;
    Services.getUsers().then((users) {
      setState(() {
        _users = users;
        _loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(_loading ? 'Loading...' : 'Api Demo'),
      ),
      body: Container(
        color: Colors.white,
        child: ListView.builder(
            itemCount: _users == null ? 0 : _users.length,
            itemBuilder: (context, index) {
              User user = _users[index];
              return ListTile(
                title: Text(user.name),
                subtitle: Text(user.email),
              );
            }),
      ),
    );
  }
}
