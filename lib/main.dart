import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart'; // Add this line.
import 'package:sampleapp/pages/LoginPage.dart';
import 'package:sampleapp/utils/colors.dart';
import 'pages/RandomWords.dart';
import './routes/Routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
//    final wordPair = WordPair.random(); // Add this line.
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        print("FOCUS:"+currentFocus.hasPrimaryFocus.toString());
        if (!currentFocus.hasPrimaryFocus) {
//          currentFocus.unfocus();
          WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
        }
      },
      child:MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
//          primarySwatch: Colors.indigo,
//        primaryColor: Color(0xFF4d7093),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
          primarySwatch: AppColor.themDark,
          primaryColorDark: AppColor.themDark,
          accentColor: AppColor.themDark,
        ),
//        home: LoginPage(),
        routes: customRoutes,
      ),
    );
  }
}
