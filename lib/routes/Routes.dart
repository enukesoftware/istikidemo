import 'package:flutter/material.dart';
import 'package:sampleapp/apidemo/ApiDemo.dart';
import 'package:sampleapp/pages/AddItemPage.dart';
import 'package:sampleapp/pages/HomePage.dart';
import 'package:sampleapp/pages/ItemListPage.dart';
import 'package:sampleapp/pages/LoginPage.dart';
import 'package:sampleapp/pages/MyCameraPage.dart';
import 'package:sampleapp/pages/PreviewImagePage.dart';
import 'package:sampleapp/pages/PreviewVideoPage.dart';
import 'package:sampleapp/pages/RandomWords.dart';
import 'package:sampleapp/pages/SplashScreenPage.dart';
import 'package:sampleapp/routes/RouteConstants.dart';

var customRoutes = <String, WidgetBuilder>{
  '/': (context) => SplashScreenPage(),
  SplashRoute: (context) => SplashScreenPage(),
  LoginPageRoute: (context) => LoginPage(),
  HomePageRoute: (context) => HomePage(),
  AddItemPageRoute: (context) => AddItemPage(),
  PreviewImagePageRoute: (context) => PreviewImagePage(),
  PreviewVideoPageRoute: (context) => PreviewVideoPage(),
  RandomWordsRoute: (context) => RandomWords(),
  ApiDemoRoute: (context) => ApiDemo(),
  ItemListPageRoute: (context) => ItemListPage(),
  CameraPageRoute: (context) => MyCameraPage(),
};
