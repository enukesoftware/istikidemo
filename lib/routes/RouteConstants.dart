const String SplashRoute = '/splashscreenpage';
const String LoginPageRoute = '/loginpage';
const String HomePageRoute = '/homepage';
const String AddItemPageRoute = '/additempage';
const String PreviewImagePageRoute = '/previewimagepage';
const String PreviewVideoPageRoute = '/previewvideopage';
const String RandomWordsRoute = '/randomwords';
const String ApiDemoRoute = '/apidemo';
const String ItemListPageRoute = '/itemlistpage';
const String CameraPageRoute = '/camerapage';
const String BACK="back";